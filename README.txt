
Blindtextgenerator 1.x for Drupal 6.x
-------------------------------------
The module embeds lorem-ipsum texts from www.blindtextgenerator.com into textareas via Javascript.
You need this if you want to generate dummy texts for testing purposes.


Installation
------------
Blindtextgenerator can be installed like any other Drupal module -- place it in
the modules directory for your site and enable it.


Configuration
-------------
Go to the module settings page (admin/settings/blindtextgenerator).
You can configure for which textareas the module should be ready (nodes, comments, blocks)
and / or you can configure additional textareas by their form IDs.


Deinstallation
--------------
- Disable module
- Uninstall module
All variables in the database are going to be completely removed.
Delete the module folder in modules folder.


Supported text editors
----------------------

Fully supported:
- Simple Drupal text area
- CKEditor
- FCKeditor
- Whizzywig
- nicEdit
- YUI Rich Text Editor
- jWYSIWYG
- WYMeditor

Partly supported:
- TinyMCE(no paragraphs)


Maintainers
-----------

- sachbearbeiter (www.diesachbearbeiter.de or http://drupal.org/user/86879)


